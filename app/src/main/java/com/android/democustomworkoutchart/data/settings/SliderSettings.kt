package com.android.democustomworkoutchart.data.settings

import android.content.res.TypedArray
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import com.android.democustomworkoutchart.R


class SliderSettings() {

    companion object {
        fun fromTypedArray(typedArray: TypedArray) = SliderSettings(typedArray)
    }

    @ColorInt
    var sliderBackgroundColor = Color.TRANSPARENT

    @ColorInt
    var sliderBackgroundChartColor = Color.TRANSPARENT

    @DrawableRes
    var sliderButtonLeftImageSrc = 0

    @DrawableRes
    var sliderButtonRightImageSrc = 0

    @DrawableRes
    var sliderButtonLeftSelectedImageSrc = 0

    @DrawableRes
    var sliderButtonRightSelectedImageSrc = 0

    @ColorInt
    var sliderLeftEdgeColor = Color.TRANSPARENT

    @ColorInt
    var sliderRightEdgeColor = Color.TRANSPARENT

    @ColorInt
    var sliderLeftEdgeSelectedColor = Color.TRANSPARENT

    @ColorInt
    var sliderRightEdgeSelectedColor = Color.TRANSPARENT

    @ColorInt
    var sliderRangeAreaBackgroundColor = Color.TRANSPARENT

    @ColorInt
    var sliderRangeAreaPressedBackgroundColor = Color.TRANSPARENT

    var sliderTouchMode: SliderTouchMode = SliderTouchMode.MODE_DRAG_AND_DROP

    private constructor(typedArray: TypedArray) : this() {

        sliderBackgroundColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderBackgroundColor,
            sliderBackgroundColor
        )

        sliderBackgroundChartColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderBackgroundChartColor,
            sliderBackgroundChartColor
        )

        sliderButtonLeftImageSrc = typedArray.getResourceId(
            R.styleable.GraphLayout_gl_sliderLeftButtonImageResource,
            sliderButtonLeftImageSrc
        )

        sliderButtonRightImageSrc = typedArray.getResourceId(
            R.styleable.GraphLayout_gl_sliderRightButtonImageResource,
            sliderButtonRightImageSrc
        )

        sliderButtonLeftSelectedImageSrc = typedArray.getResourceId(
            R.styleable.GraphLayout_gl_sliderLeftButtonSelectedImageResource,
            sliderButtonLeftSelectedImageSrc
        )

        sliderButtonRightSelectedImageSrc = typedArray.getResourceId(
            R.styleable.GraphLayout_gl_sliderRightButtonSelectedImageResource,
            sliderButtonRightSelectedImageSrc
        )

        sliderLeftEdgeColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderLeftEdgeColor,
            sliderLeftEdgeColor
        )

        sliderRightEdgeColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderRightEdgeColor,
            sliderRightEdgeColor
        )

        sliderLeftEdgeSelectedColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderLeftEdgeSelectedColor,
            sliderLeftEdgeSelectedColor
        )

        sliderRightEdgeSelectedColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderRightEdgeSelectedColor,
            sliderRightEdgeSelectedColor
        )

        sliderRangeAreaBackgroundColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderRangeAreaBackgroundColor,
            sliderRangeAreaBackgroundColor
        )

        sliderRangeAreaPressedBackgroundColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_sliderRangeAreaPressedBackgroundColor,
            sliderRangeAreaPressedBackgroundColor
        )

        sliderTouchMode = SliderTouchMode.values()[
                typedArray.getInt(
                    R.styleable.GraphLayout_gl_sliderTouchMode,
                    sliderTouchMode.value
                )
        ]

    }

    enum class SliderTouchMode(val value: Int) {
        MODE_CLOSEST_EDGE(0),
        MODE_DRAG_AND_DROP(1)
    }
}