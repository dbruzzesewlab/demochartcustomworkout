package com.android.democustomworkoutchart.data.settings

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.text.TextPaint
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import com.android.democustomworkoutchart.R
import com.android.democustomworkoutchart.data.MaskOverFtp

class FtpSettings() {

    companion object {
        fun fromTypedArray(
            context: Context,
            typedArray: TypedArray
        ) = FtpSettings(context, typedArray)
    }

    var ftpMaskOver: MaskOverFtp? = null

    @ColorInt
    var ftpTextColor = Color.TRANSPARENT

    @ColorInt
    var ftpTextBackgroundColor = Color.TRANSPARENT

    @Dimension(unit = Dimension.SP)
    var ftpTextSize = 12f

    var avgFtp = 0

    var maxFtpPeak = 300

    @Dimension(unit = Dimension.DP)
    var ftpTextLineSpacing = 0f

    @Dimension(unit = Dimension.DP)
    var ftpTextPadding = 0f

    var isFtpTextVisible = false

    var isFtpStrokeVisible = false

    @Dimension(unit = Dimension.DP)
    var ftpStrokeWidth = 0f

    var ftpTypeface: Typeface? = null

    var ftpText = ""

    val ftpTextPaint: TextPaint
        get() = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
            this.color = ftpTextColor
            this.textSize = ftpTextSize
            this.typeface = ftpTypeface
        }

    var ftpTextWidthOffset = 0f

    var ftpTextHeightOffset = 0f

    private constructor(context: Context, typedArray: TypedArray) : this() {
        ftpTextColor = typedArray.getColor(R.styleable.GraphLayout_gl_ftpTextColor, ftpTextColor)

        ftpTextBackgroundColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_ftpTextBackgroundColor,
            ftpTextBackgroundColor
        )

        ftpTextSize = typedArray.getDimension(R.styleable.GraphLayout_gl_ftpTextSize, ftpTextSize)

        avgFtp = typedArray.getInt(R.styleable.GraphLayout_gl_ftpAvg, avgFtp)

        maxFtpPeak = typedArray.getInt(R.styleable.GraphLayout_gl_ftpMaxPeak, maxFtpPeak)

        ftpTextLineSpacing = typedArray.getDimension(
            R.styleable.GraphLayout_gl_ftpTextLineSpacing,
            ftpTextLineSpacing
        )

        ftpTextPadding = typedArray.getDimension(
            R.styleable.GraphLayout_gl_ftpTextPadding,
            ftpTextPadding
        )

        isFtpTextVisible = typedArray.getBoolean(
            R.styleable.GraphLayout_gl_ftpTextVisible,
            isFtpTextVisible
        )

        ftpTypeface = kotlin.runCatching {
            Typeface.createFromAsset(
                context.assets,
                typedArray.getString(R.styleable.GraphLayout_gl_ftpTypeface)
            )
        }.getOrNull()

        ftpText = typedArray.getString(R.styleable.GraphLayout_gl_ftpText) ?: ""

        if (typedArray.hasValue(R.styleable.GraphLayout_gl_ftpStrokeWidth)) {
            ftpStrokeWidth = typedArray.getDimension(
                R.styleable.GraphLayout_gl_ftpStrokeWidth,
                ftpStrokeWidth
            )
        }
    }
}