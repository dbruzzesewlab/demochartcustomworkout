package com.android.democustomworkoutchart.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.android.democustomworkoutchart.Utils
import com.android.democustomworkoutchart.data.GraphData
import com.android.democustomworkoutchart.data.settings.SliderSettings
import kotlin.math.abs


@Suppress("ktNoinlineFunc")
class SliderView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    interface OnSliderRangeChangedListener {
        fun onSliderRangeChanged(startRange: Float, endRange: Float)
    }

    var onSliderRangeChangedListener: OnSliderRangeChangedListener? = null
    var sliderSettings: SliderSettings = SliderSettings()
    val portions = mutableListOf<GraphData>()
    var maxFtpPeak = 0
    var totalTime = 0

    private var sideTouching = Side.NONE

    private var leftDelta = 0f
    private var rightDelta = 0f
    private var tempLeftDelta = 0f
    private var tempRightDelta = 0f
    private var translationDelta = 0f

    private var leftPositionX = 0f
    private var rightPositionX = 0f

    private val widthF: Float
        get() = width.toFloat()

    private val heightF: Float
        get() = height.toFloat()

    private val side: Float
        get() = heightF

    private val bitmapLeftButton: Bitmap? by lazy {
        Utils.getBitmapFromVectorDrawable(
            context,
            sliderSettings.sliderButtonLeftImageSrc,
            side.toInt(),
            side.toInt()
        )
    }

    private val bitmapRightButton: Bitmap? by lazy {
        Utils.getBitmapFromVectorDrawable(
            context,
            sliderSettings.sliderButtonRightImageSrc,
            side.toInt(),
            side.toInt()
        )
    }

    private val bitmapLeftButtonSelected: Bitmap? by lazy {
        Utils.getBitmapFromVectorDrawable(
            context,
            sliderSettings.sliderButtonLeftSelectedImageSrc,
            side.toInt(),
            side.toInt()
        )
    }

    private val bitmapRightButtonSelected: Bitmap? by lazy {
        Utils.getBitmapFromVectorDrawable(
            context,
            sliderSettings.sliderButtonRightSelectedImageSrc,
            side.toInt(),
            side.toInt()
        )
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {

                when (sliderSettings.sliderTouchMode) {

                    SliderSettings.SliderTouchMode.MODE_DRAG_AND_DROP -> {
                        when {
                            isLeftButtonTouched(event) -> {
                                sideTouching = Side.LEFT
                            }
                            isRightButtonTouched(event) -> {
                                sideTouching = Side.RIGHT
                            }
                            isMiddleTouched(event) -> {
                                translationDelta = event.x
                                tempLeftDelta = leftDelta
                                tempRightDelta = rightDelta
                                sideTouching = Side.MIDDLE
                            }
                        }
                    }

                    SliderSettings.SliderTouchMode.MODE_CLOSEST_EDGE -> {
                        sideTouching = if (getClosestButton(event.x) == Side.LEFT) {
                            updateButtonLeftPosition(event)
                            Side.LEFT
                        } else {
                            updateButtonRightPosition(event)
                            Side.RIGHT
                        }

                        invalidate()
                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when (sideTouching) {
                    Side.LEFT -> {
                        updateButtonLeftPosition(event)
                    }
                    Side.RIGHT -> {
                        updateButtonRightPosition(event)
                    }
                    Side.MIDDLE -> {
                        updateMiddlePosition(event)
                    }
                    else -> {
                        // Do nothing
                    }
                }
                invalidate()
                return true
            }
            MotionEvent.ACTION_UP -> {
                sideTouching = Side.NONE
                translationDelta = 0f
                tempLeftDelta = 0f
                tempRightDelta = 0f
                invalidate()
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    private fun isMiddleTouched(event: MotionEvent) = event.x in leftDelta..(widthF - rightDelta)

    private fun isRightButtonTouched(event: MotionEvent) =
        event.x in ((widthF - rightDelta) - side)..(widthF - rightDelta) + side

    private fun isLeftButtonTouched(event: MotionEvent) = event.x in leftDelta..(leftDelta + side)

    private fun getClosestButton(x: Float): Side {
        val leftDifference = abs(leftPositionX - x)
        val rightDifference = abs(rightPositionX - x)
        return if (leftDifference <= rightDifference)
            Side.LEFT
        else
            Side.RIGHT
    }

    private fun updateMiddlePosition(event: MotionEvent) {
        val translation = translationDelta - event.x
        if (translation > 0) {
            leftDelta = tempLeftDelta - translation
            if (leftDelta < 0) {
                leftDelta = 0f
                return
            }
            rightDelta = tempRightDelta + translation
        } else {
            rightDelta = tempRightDelta + translation
            if (rightDelta < 0) {
                rightDelta = 0f
                return
            }
            leftDelta = tempLeftDelta - translation
        }
    }

    private fun updateButtonLeftPosition(event: MotionEvent) {
        leftDelta = event.x - (side / 2)
        if (leftDelta < 0) leftDelta = 0f
        if (widthF - leftDelta < rightDelta + (side * 2))
            leftDelta = (widthF - rightDelta) - (side * 2)
    }

    private fun updateButtonRightPosition(event: MotionEvent) {
        rightDelta = widthF - event.x - (side / 2)
        if (rightDelta < 0) rightDelta = 0f
        if (widthF - rightDelta < leftDelta + (side * 2))
            rightDelta = (widthF - leftDelta) - (side * 2)
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawColor(sliderSettings.sliderBackgroundColor)

        var startX = 0f
        var endX = 0f

        portions.forEachIndexed { _, data ->
            endX += ((widthF * data.time) / (totalTime))
            canvas.drawRect(
                startX,
                (heightF - ((heightF * data.ftp) / maxFtpPeak)),
                endX,
                heightF,
                Paint().apply {
                    this.style = Paint.Style.FILL
                    this.color = sliderSettings.sliderBackgroundChartColor
                }
            )

            startX = endX
        }

        leftPositionX = leftDelta
        rightPositionX = widthF - (rightDelta + side)

        // Draw the selected area
        canvas.drawRect(
            leftPositionX,
            0f,
            rightPositionX + side,
            heightF,
            Paint().apply {
                this.color = if (sideTouching == Side.NONE)
                    sliderSettings.sliderRangeAreaBackgroundColor
                else
                    sliderSettings.sliderRangeAreaPressedBackgroundColor
            }
        )

        // Draw left button
        drawLeftButton(canvas)

        // Draw right button
        drawRightButton(canvas)

        val startRange = (totalTime * leftDelta) / widthF
        val endRange = totalTime - (totalTime * rightDelta) / widthF
        onSliderRangeChangedListener?.onSliderRangeChanged(startRange, endRange)
    }

    private fun drawLeftButton(canvas: Canvas) {

        canvas.drawLine(
            leftPositionX,
            0f,
            leftPositionX,
            heightF,
            Paint().apply {
                this.color = if (sideTouching == Side.LEFT)
                    sliderSettings.sliderLeftEdgeSelectedColor
                else
                    sliderSettings.sliderLeftEdgeColor
            }
        )

        val buttonBitmap =
            if (sideTouching != Side.NONE && bitmapLeftButtonSelected != null) {
                bitmapLeftButtonSelected
            } else {
                bitmapLeftButton
            }

        buttonBitmap?.let {
            canvas.drawBitmap(it, leftPositionX, 0f, Paint())
        }
    }

    private fun drawRightButton(canvas: Canvas) {

        canvas.drawLine(
            rightPositionX + side - 1,
            0f,
            rightPositionX + side - 1,
            heightF,
            Paint().apply {
                this.color = if (sideTouching == Side.RIGHT)
                    sliderSettings.sliderRightEdgeSelectedColor
                else
                    sliderSettings.sliderRightEdgeColor
            }
        )

        val buttonBitmap =
            if (sideTouching != Side.NONE && bitmapRightButtonSelected != null) {
                bitmapRightButtonSelected
            } else {
                bitmapRightButton
            }

        buttonBitmap?.let {
            canvas.drawBitmap(it, rightPositionX, 0f, Paint())
        }
    }


    private enum class Side {
        LEFT,
        RIGHT,
        MIDDLE,
        NONE
    }
}