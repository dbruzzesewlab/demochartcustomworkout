package com.android.democustomworkoutchart.data

data class GraphData(
    var index: Int,
    var time: Int,
    val ftp: Int,
    val rpm: Int = -1
)

data class CompletedGraphData(
    val graphData: GraphData,
)
