package com.android.democustomworkoutchart.data.settings

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import com.android.democustomworkoutchart.R

class IndexSettings() {

    companion object {
        fun fromTypedArray(
            context: Context,
            typedArray: TypedArray
        ) = IndexSettings(context, typedArray)
    }

    @ColorInt
    var indexTextColor = Color.TRANSPARENT

    @ColorInt
    var indexSelectedTextColor = Color.TRANSPARENT

    @Dimension(unit = Dimension.SP)
    var indexTextSize = 9f

    @Dimension(unit = Dimension.DP)
    var indexMarginTop = 0f

    var indexTypeface: Typeface? = null

    private constructor(context: Context, typedArray: TypedArray) : this() {
        indexTextColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_indexTextColor,
            indexTextColor
        )

        indexSelectedTextColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_indexSelectedTextColor,
            indexSelectedTextColor
        )

        indexTextSize = typedArray.getDimension(
            R.styleable.GraphLayout_gl_indexTextSize,
            indexTextSize
        )

        indexMarginTop = typedArray.getDimension(
            R.styleable.GraphLayout_gl_indexMarginTop,
            indexMarginTop
        )

        indexTypeface = kotlin.runCatching {
            Typeface.createFromAsset(
                context.assets,
                typedArray.getString(
                    R.styleable.GraphLayout_gl_indexTypeface
                )
            )
        }.getOrNull()
    }
}