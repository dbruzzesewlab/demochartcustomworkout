package com.android.democustomworkoutchart.data

import androidx.annotation.ColorInt

abstract class RangeColor(
    open val min: Int,
    open val max: Int
) {

    data class FromColor(
        @ColorInt val color: Int,
        override val min: Int,
        override val max: Int,
    ) : RangeColor(min, max)

    data class FromGradient(
        @ColorInt val colorStart: Int,
        @ColorInt val colorEnd: Int,
        override val min: Int,
        override val max: Int,
    ) : RangeColor(min, max)
}