package com.android.democustomworkoutchart

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.android.democustomworkoutchart.data.GraphData
import com.android.democustomworkoutchart.data.MaskOverFtp
import com.android.democustomworkoutchart.data.RangeColor
import com.android.democustomworkoutchart.data.settings.SliderSettings
import com.android.democustomworkoutchart.widgets.GraphLayout
import kotlin.random.Random

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val graphLayout = findViewById<GraphLayout>(R.id.graphLayout)

        graphLayout.setRangeColors(
            RangeColor.FromGradient(
                colorStart = ContextCompat.getColor(this, R.color.z1_color_start),
                colorEnd = ContextCompat.getColor(this, R.color.z1_color_end),
                min = resources.getInteger(R.integer.z1_min),
                max = resources.getInteger(R.integer.z1_max),
            ),
            RangeColor.FromGradient(
                colorStart = ContextCompat.getColor(this, R.color.z2_color_start),
                colorEnd = ContextCompat.getColor(this, R.color.z2_color_end),
                min = resources.getInteger(R.integer.z2_min),
                max = resources.getInteger(R.integer.z2_max),
            ),
            RangeColor.FromGradient(
                colorStart = ContextCompat.getColor(this, R.color.z3_color_start),
                colorEnd = ContextCompat.getColor(this, R.color.z3_color_end),
                min = resources.getInteger(R.integer.z3_min),
                max = resources.getInteger(R.integer.z3_max),
            ),
            RangeColor.FromGradient(
                colorStart = ContextCompat.getColor(this, R.color.z4_color_start),
                colorEnd = ContextCompat.getColor(this, R.color.z4_color_end),
                min = resources.getInteger(R.integer.z4_min),
                max = resources.getInteger(R.integer.z4_max),
            ),
            RangeColor.FromGradient(
                colorStart = ContextCompat.getColor(this, R.color.z5_color_start),
                colorEnd = ContextCompat.getColor(this, R.color.z5_color_end),
                min = resources.getInteger(R.integer.z5_min),
                max = resources.getInteger(R.integer.z5_max),
            )
        )


        // region GenericSettings

        // Could use gv_alternateBackgroundColor1 and gv_alternateBackgroundColor2
        graphLayout.setAlternateBackgroundColors(
            color1 = ContextCompat.getColor(this, R.color.alternate_background_color_1),
            color2 = ContextCompat.getColor(this, R.color.alternate_background_color_2)
        )

        // Could use gv_spaceBetweenItems
        graphLayout.setSpaceBetweenItems(2f)

        // Could use gv_spaceBetweenChartAndSlider
        graphLayout.setSpaceBetweenChartAndSlider(10f)

        // Could use gv_dashStrokeWidth
        graphLayout.setDashHeight(2f)

        // Could use gl_dashVisibleWidth
        graphLayout.setDashVisibleWidth(3f)

        // Could use gl_dashSpacingWidth
        graphLayout.setDashSpacingWidth(9f)

        // Could use gv_dashColor
        graphLayout.setDashColor(Color.parseColor("#4dffffff"))

        // Could use gv_selectedColor
        graphLayout.setSelectedColor(Color.parseColor("#40ffffff"))

        // Could use gv_selectedSeparatorColor
        graphLayout.setSelectedSeparatorColor(ContextCompat.getColor(this, R.color.tg_yellow))

        // Could use gv_selectedPosition
        graphLayout.setSelectedItemAtIndex(2)

        val rand = Random(300)
        repeat(40) {
            graphLayout.addPortion(
                GraphData(
                    index = it + 1,
                    time = rand.nextInt(60, 600),
                    ftp = rand.nextInt(10, 200),
                    rpm = rand.nextInt(-20, 150)
                )
            )
        }

        // endregion GenericSettings



        // region FTP

        // Could use gl_ftpTypeface
        graphLayout.setFtpTypeface(getString(R.string.ftp_typeface))

        graphLayout.setFtpMaskOver(
            MaskOverFtp(
                colorStart = ContextCompat.getColor(this, R.color.mask_color_start),
                colorEnd = ContextCompat.getColor(this, R.color.mask_color_end),
                alpha = 0.8f
            )
        )

        // Could use gl_ftpTextBackgroundColor
        graphLayout.setFtpTextBackgroundColor(ContextCompat.getColor(this, R.color.dark_grey))

        // Could use gv_ftpTextColor
        graphLayout.setFtpTextColor(Color.parseColor("#99ffffff"))

        // Could use gv_maxFtp
        graphLayout.setFtpMaxPeak(300)

        // Could use gv_ftpStrokeWidth
        graphLayout.showFtpStroke(1f)

        // Could use gv_ftpTextLineSpacing
        graphLayout.setFtpTextLineSpacing(5f)

        // Could use gv_ftpTextPadding
        graphLayout.setFtpTextPadding(8f)

        // Could use gv_ftpTextVisible
        graphLayout.setFtpTextVisible(true)

        // Could use gv_ftpAvg
        graphLayout.setFtpAvg(150)

        // Could use gv_ftpTextSize
        graphLayout.setFtpTextSize(12f)

        // Could use gl_ftpText
        graphLayout.setFtpText("FTP\n150\nW")

        // endregion FTP



        // region RPM

        // Could use gl_rpmTypeface
        graphLayout.setRpmTypeface(getString(R.string.rpm_typeface))

        // Could use gv_rpmLineColor
        graphLayout.setRpmLineColor(Color.parseColor("#99ffffff"))

        // Could use gv_rpmMax
        graphLayout.setRpmMax(150)

        // Could use gv_rpmTextColor
        graphLayout.setRpmTextColor(ContextCompat.getColor(this, R.color.tg_grey))

        // Could use gv_rpmLineHeight
        graphLayout.setRpmLineHeight(3f)

        // Could use gv_rpmBoxColor
        graphLayout.setRpmBoxColor(Color.parseColor("#99ffffff"))

        // Could use gv_rpmTextSize
        graphLayout.setRpmTextSize(12f)

        // Could use gv_rpmBoxRoundCorners
        graphLayout.setRpmBoxRoundCorners(6f)

        // Could use gv_rpmBoxMarginStart
        graphLayout.setRpmBoxMarginStart(6f)

        // Could use gv_rpmBoxPadding
        graphLayout.setRpmBoxPadding(2f)

        // Could use gv_rpmSelectedLineColor
        graphLayout.setRpmSelectedLineColor(Color.WHITE)

        // Could use gv_rpmSelectedBoxColor
        graphLayout.setRpmSelectedBoxColor(Color.WHITE)

        // endregion RPM


        // region Index

        // Could use gv_indexTextColor
        graphLayout.setIndexTextColor(Color.parseColor("#99ffffff"))

        // Could use gv_indexTextSize
        graphLayout.setIndexTextSize(24f)

        // Could use gv_indexSelectedTextColor
        graphLayout.setIndexSelectedTextColor(Color.WHITE)

        // Could use gv_indexMarginTop
        graphLayout.setIndexMarginTop(5f)

        // Could use gl_indexTypeface
        graphLayout.setIndexTypeface(getString(R.string.index_typeface))

        // endregion Index


        // region Slider

        // Could use gv_sliderBackgroundColor
        graphLayout.setSliderBackgroundColor(ContextCompat.getColor(this, R.color.dark_grey))

        // Could use gl_sliderBackgroundChartColor
        graphLayout.setSliderBackgroundChartColor(Color.parseColor("#3e3e41"))

        // Could use gl_sliderLeftButtonImageResource
        graphLayout.setSliderButtonLeftImageResource(R.drawable.ic_slider_button)

        // Could use gl_sliderRightButtonImageResource
        graphLayout.setSliderButtonRightImageResource(R.drawable.ic_slider_button)

        // Could use gl_sliderLeftButtonSelectedImageResource
        graphLayout.setSliderButtonLeftSelectedImageResource(R.drawable.ic_slider_button_selected)

        // Could use gl_sliderRightButtonSelectedImageResource
        graphLayout.setSliderButtonRightSelectedImageResource(R.drawable.ic_slider_button_selected)

        // Could use gl_sliderLeftEdgeColor
        graphLayout.setSliderEdgeLeftColor(Color.parseColor("#26ffffff"))

        // Could use gl_sliderRightEdgeColor
        graphLayout.setSliderEdgeRightColor(Color.parseColor("#26ffffff"))

        // Could use gl_sliderLeftEdgeSelectedColor
        graphLayout.setSliderEdgeLeftSelectedColor(ContextCompat.getColor(this, R.color.tg_yellow))

        // Could use gl_sliderRightEdgeSelectedColor
        graphLayout.setSliderEdgeRightSelectedColor(ContextCompat.getColor(this, R.color.tg_yellow))

        // Could use gl_sliderSingleRangeSelectedBackgroundColor
        graphLayout.setSliderRangeAreaBackgroundColor(Color.parseColor("#26ffffff"))

        // Could use gl_sliderDoubleRangeSelectedBackgroundColor
        graphLayout.setSliderRangeAreaPressedBackgroundColor(Color.parseColor("#26ffe01e"))

        // Could use gl_sliderTouchMode
        graphLayout.setSliderTouchMode(SliderSettings.SliderTouchMode.MODE_DRAG_AND_DROP)

        // endregion Slider
    }
}