package com.android.democustomworkoutchart.extensions

import android.graphics.Paint
import android.graphics.Rect
import android.util.SizeF

/**
 * Get the width and height of a text painted on canvas.
 * @param text the text to paint.
 * @return a pair with the width and height.
 */
fun Paint.getTextMeasureSize(text: String): SizeF {
    val bounds = Rect()
    this.getTextBounds(text, 0, text.length, bounds)
    return SizeF(
        this.measureText(text),
        bounds.height().toFloat()
    )
}

fun Paint.getTextMeasureSize(text: String, padding: Float): SizeF {
    val sizeF = this.getTextMeasureSize(text)
    return SizeF(
        sizeF.width + 2 * padding,
        sizeF.height + 2 * padding
    )
}