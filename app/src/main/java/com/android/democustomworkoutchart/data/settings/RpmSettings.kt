package com.android.democustomworkoutchart.data.settings

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import com.android.democustomworkoutchart.R

class RpmSettings() {

    companion object {
        fun fromTypedArray(
            context: Context,
            typedArray: TypedArray
        ) = RpmSettings(context, typedArray)
    }

    var rpmMin: Int = 0

    var rpmMax = 0

    @ColorInt
    var rpmLineColor = Color.TRANSPARENT

    @Dimension
    var rpmLineHeight = 1f

    @ColorInt
    var rpmTextColor = Color.TRANSPARENT

    @Dimension(unit = Dimension.SP)
    var rpmTextSize = 9f

    @ColorInt
    var rpmBoxColor = Color.TRANSPARENT

    @Dimension(unit = Dimension.DP)
    var rpmBoxRoundCorners = 0f

    @Dimension(unit = Dimension.DP)
    var rpmBoxMarginStart = 0f

    @Dimension(unit = Dimension.DP)
    var rpmBoxPadding = 0f

    @ColorInt
    var rpmSelectedLineColor = Color.TRANSPARENT

    @ColorInt
    var rpmSelectedBoxColor = Color.TRANSPARENT

    var rpmTypeface: Typeface? = null

    private constructor(context: Context, typedArray: TypedArray) : this() {
        rpmMin = typedArray.getInt(
            R.styleable.GraphLayout_gl_rpmMin,
            rpmMin
        )

        rpmMax = typedArray.getInt(
            R.styleable.GraphLayout_gl_rpmMax,
            rpmMax
        )

        rpmLineColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_rpmLineColor,
            rpmLineColor
        )

        rpmLineHeight = typedArray.getDimension(
            R.styleable.GraphLayout_gl_rpmLineHeight,
            rpmLineHeight
        )

        rpmTextColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_rpmTextColor,
            rpmTextColor
        )

        rpmTextSize = typedArray.getDimension(
            R.styleable.GraphLayout_gl_rpmTextSize,
            rpmTextSize
        )

        rpmBoxColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_rpmBoxColor,
            rpmBoxColor
        )

        rpmBoxRoundCorners = typedArray.getDimension(
            R.styleable.GraphLayout_gl_rpmBoxRoundCorners,
            rpmBoxRoundCorners
        )

        rpmBoxMarginStart = typedArray.getDimension(
            R.styleable.GraphLayout_gl_rpmBoxMarginStart,
            rpmBoxMarginStart
        )

        rpmBoxPadding = typedArray.getDimension(
            R.styleable.GraphLayout_gl_rpmBoxPadding,
            rpmBoxPadding
        )

        rpmSelectedLineColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_rpmSelectedLineColor,
            rpmSelectedLineColor
        )

        rpmSelectedBoxColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_rpmSelectedBoxColor,
            rpmSelectedBoxColor
        )

        rpmTypeface = kotlin.runCatching {
            Typeface.createFromAsset(
                context.assets,
                typedArray.getString(
                    R.styleable.GraphLayout_gl_rpmTypeface
                )
            )
        }.getOrNull()
    }
}