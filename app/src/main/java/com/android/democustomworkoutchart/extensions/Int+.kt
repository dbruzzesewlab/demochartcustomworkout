package com.android.democustomworkoutchart

fun Int.isEven() = this % 2 == 0