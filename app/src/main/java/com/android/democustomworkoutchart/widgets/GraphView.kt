package com.android.democustomworkoutchart.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.Shader.TileMode
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import com.android.democustomworkoutchart.data.CompletedGraphData
import com.android.democustomworkoutchart.data.GraphData
import com.android.democustomworkoutchart.data.RangeColor
import com.android.democustomworkoutchart.data.settings.FtpSettings
import com.android.democustomworkoutchart.data.settings.GenericSettings
import com.android.democustomworkoutchart.data.settings.IndexSettings
import com.android.democustomworkoutchart.data.settings.RpmSettings
import com.android.democustomworkoutchart.extensions.getTextMeasureSize
import com.android.democustomworkoutchart.isEven

@Suppress("ktNoinlineFunc")
class GraphView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    var genericSettings: GenericSettings = GenericSettings()
    var rpmSettings: RpmSettings = RpmSettings()
    var ftpSettings: FtpSettings = FtpSettings()
    var indexSettings: IndexSettings = IndexSettings()

    private val portions = mutableListOf<CompletedGraphData>()
    private val tmpPortions = mutableListOf<CompletedGraphData>()

    var totalTime = 0
    var rangeColors = listOf<RangeColor>()

    // Item index, Sum time
    private val mapTimes = mutableMapOf<Int, Float>()

    private val heightOffset: Float
        get() = indexSettings.indexTextSize * 2

    private val widthF: Float
        get() = width.toFloat()

    private val heightF: Float
        get() = height.toFloat() - heightOffset

    fun addItem(graphData: GraphData) {
        portions.add(
            CompletedGraphData(
                graphData = graphData,
            )
        )
        tmpPortions.add(
            CompletedGraphData(
                graphData = graphData,
            )
        )
        mapTimes[portions.size - 1] = portions.sumOf { it.graphData.time }.toFloat()
    }

    fun updateRange(startRange: Float, endRange: Float) {
        portions.clear()
        var startTime = 0f

        mapTimes.forEach { (index, endTime) ->

            val graphData = GraphData(
                index = this.tmpPortions[index].graphData.index,
                time = this.tmpPortions[index].graphData.time,
                ftp = this.tmpPortions[index].graphData.ftp,
                rpm = this.tmpPortions[index].graphData.rpm
            )

            if (endTime in startRange..endRange) {

                if (startRange in startTime..endTime) {
                    graphData.time = (endTime - startRange).toInt()
                }
                portions.add(
                    CompletedGraphData(
                        graphData = graphData,
                    )
                )
            } else if (endRange in startTime..endTime) {

                graphData.time = (endRange - startTime).toInt()
                portions.add(
                    CompletedGraphData(
                        graphData = graphData,
                    )
                )
            }

            startTime = endTime
        }
        totalTime = portions.sumOf {
            it.graphData.time
        }
        invalidate()
    }

    private fun isSelectedItem(index: Int): Boolean {
        return genericSettings.selectedItemAtIndex == portions.getOrNull(index)?.graphData?.index
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas ?: return

        var cellStartX = 0f
        var cellEndX = 0f

        var ftpRectTop: Float

        var ftpStrokeColor = Color.TRANSPARENT

        val avgFtp =
            (heightF - ((heightF * ftpSettings.avgFtp) / ftpSettings.maxFtpPeak)) + heightOffset

        portions.forEachIndexed { index, data ->

            // Draw the rectangle container
            cellEndX += ((((widthF - ftpSettings.ftpTextWidthOffset) * data.graphData.time)) / (totalTime))
            canvas.drawRect(
                cellStartX,
                0f,
                cellEndX,
                heightF + heightOffset,
                Paint().apply {
                    color = when {
                        data.graphData.index.isEven() -> genericSettings.alternateBackgroundColor1
                        else -> genericSettings.alternateBackgroundColor2
                    }
                })

            // Draw the ftp
            ftpRectTop = (heightF * data.graphData.ftp) / ftpSettings.maxFtpPeak
            val range = rangeColors.find {
                data.graphData.ftp in it.min..it.max
            }
            canvas.drawRect(
                cellStartX,
                (heightF - ftpRectTop) + heightOffset,
                cellEndX,
                heightF + heightOffset,
                Paint().apply {
                    style = Paint.Style.FILL
                    when (range) {
                        is RangeColor.FromColor -> {
                            color = range.color
                            ftpStrokeColor = range.color
                        }
                        is RangeColor.FromGradient -> {
                            this.shader = LinearGradient(
                                0f, heightF - ftpRectTop, 0f, heightF,
                                range.colorStart,
                                range.colorEnd,
                                TileMode.CLAMP
                            )
                            ftpStrokeColor = range.colorEnd
                        }
                    }
                }
            )

            // Draw the dark mask over ftp
            ftpSettings.ftpMaskOver?.let { mask ->
                canvas.drawRect(
                    cellStartX,
                    (heightF - ftpRectTop) + heightOffset,
                    cellEndX,
                    heightF + heightOffset,
                    Paint().apply {
                        this.alpha = (255 * mask.alpha).toInt()
                        this.shader = LinearGradient(
                            0f,
                            heightF - ftpRectTop,
                            0f,
                            heightF,
                            mask.colorStart,
                            mask.colorEnd,
                            TileMode.CLAMP
                        )
                    })
            }

            // Draw the FTP stroke
            if (ftpSettings.isFtpStrokeVisible) {
                canvas.drawLine(
                    cellStartX,
                    ((heightF - ftpRectTop) - (ftpSettings.ftpStrokeWidth / 2)) + heightOffset,
                    cellEndX,
                    ((heightF - ftpRectTop) - (ftpSettings.ftpStrokeWidth / 2)) + heightOffset,
                    Paint().apply {
                        style = Paint.Style.STROKE
                        strokeWidth = ftpSettings.ftpStrokeWidth
                        color = ftpStrokeColor
                    })
            }

            // Draw the RPM
            drawRpm(canvas, data.graphData, index, cellStartX, cellEndX)

            // Draw selected item
            if (isSelectedItem(index)) {
                canvas.drawRect(
                    cellStartX,
                    0f,
                    cellEndX,
                    heightF + heightOffset,
                    Paint().apply {
                        this.color = genericSettings.selectedColor
                        this.style = Paint.Style.FILL
                    })
            }

            // Draw space between items
            val startX = cellEndX - (genericSettings.spaceBetweenItems / 2)
            if (index != portions.lastIndex || ftpSettings.isFtpTextVisible) {
                canvas.drawLine(
                    startX,
                    0f,
                    startX,
                    heightF + heightOffset,
                    Paint().apply {
                        this.strokeWidth = genericSettings.spaceBetweenItems
                        this.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
                    }
                )
            }

            // Draw selected space between items
            if (isSelectedItem(index)) {
                canvas.drawLine(
                    startX,
                    0f,
                    startX,
                    heightF + heightOffset,
                    Paint().apply {
                        this.strokeWidth = genericSettings.spaceBetweenItems
                        this.color = genericSettings.selectedSeparatorColor
                        this.setShadowLayer(
                            8f,
                            0f,
                            0f,
                            genericSettings.selectedSeparatorColor
                        )
                    }
                )
            }

            // Draw index
            drawIndex(canvas, data.graphData.index, cellStartX, cellEndX)

            cellStartX = cellEndX
        }

        // Draw dashed line
        val calcLastDashX = if (ftpSettings.isFtpTextVisible) {
            genericSettings.spaceBetweenItems
        } else {
            0f
        }
        canvas.drawLine(
            0f,
            avgFtp,
            widthF - ftpSettings.ftpTextWidthOffset - calcLastDashX,
            avgFtp,
            Paint().apply {
                this.pathEffect = DashPathEffect(
                    floatArrayOf(
                        genericSettings.dashVisibleWidth,
                        genericSettings.dashSpacingWidth
                    ),
                    0f
                )
                this.style = Paint.Style.STROKE
                this.color = genericSettings.dashColor
                this.strokeWidth = genericSettings.dashHeight
            }
        )

        // Draw ftp text on the right
        if (ftpSettings.isFtpTextVisible) {
            canvas.drawRect(
                (widthF - ftpSettings.ftpTextWidthOffset),
                0f,
                widthF,
                heightF + heightOffset,
                Paint().apply {
                    this.color = ftpSettings.ftpTextBackgroundColor
                }
            )

            val staticLayout: StaticLayout = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                StaticLayout.Builder.obtain(
                    ftpSettings.ftpText,
                    0,
                    ftpSettings.ftpText.length,
                    ftpSettings.ftpTextPaint,
                    width
                )
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setLineSpacing(ftpSettings.ftpTextLineSpacing, 1F)
                    .setIncludePad(false)
                    .build()
            } else {
                StaticLayout(
                    ftpSettings.ftpText,
                    ftpSettings.ftpTextPaint,
                    width,
                    Layout.Alignment.ALIGN_NORMAL,
                    1F,
                    ftpSettings.ftpTextLineSpacing,
                    false
                )
            }
            canvas.save()
            canvas.translate(
                widthF - ftpSettings.ftpTextWidthOffset + ftpSettings.ftpTextPadding,
                avgFtp - ftpSettings.ftpTextHeightOffset / 2
            )
            staticLayout.draw(canvas)
            canvas.restore()

        }
    }

    private fun drawIndex(canvas: Canvas, index: Int, cellStartX: Float, cellEndX: Float) {

        val text = index.toString()

        val paint = TextPaint().apply {
            this.isAntiAlias = true
            this.textSize = indexSettings.indexTextSize
            this.typeface = indexSettings.indexTypeface
            this.color = if (isSelectedItem(index)) {
                indexSettings.indexSelectedTextColor
            } else {
                indexSettings.indexTextColor
            }
        }
        val textSize = paint.getTextMeasureSize(text)

        // If cell width is too little, don't show the index
        if (cellEndX - cellStartX <= textSize.width + (3f / 4f) * textSize.width) return

        val startX =
            cellStartX + ((cellEndX - cellStartX) / 2) - (textSize.width / 2) - genericSettings.spaceBetweenItems

        canvas.drawText(text, startX, (heightOffset / 2) + indexSettings.indexMarginTop, paint)
    }

    private fun drawRpm(
        canvas: Canvas,
        data: GraphData,
        index: Int,
        cellStartX: Float,
        cellEndX: Float
    ) {
        if (data.rpm <= 0) return

        val rpmY = (heightF - ((data.rpm * heightF) / rpmSettings.rpmMax)) + heightOffset

        val text = data.rpm.toString()

        val textPaint = TextPaint().apply {
            this.isAntiAlias = true
            this.color = rpmSettings.rpmTextColor
            this.textSize = rpmSettings.rpmTextSize
            this.typeface = rpmSettings.rpmTypeface
        }

        val textSize = textPaint.getTextMeasureSize(text, rpmSettings.rpmBoxPadding)

        @ColorInt val lineColor: Int
        @ColorInt val boxColor: Int
        if (isSelectedItem(index)) {
            lineColor = rpmSettings.rpmSelectedLineColor
            boxColor = rpmSettings.rpmSelectedBoxColor
        } else {
            lineColor = rpmSettings.rpmLineColor
            boxColor = rpmSettings.rpmBoxColor
        }

        val boxHeight =
            ((rpmY - (textSize.height / 2)) + textSize.height) - (rpmY - (textSize.height / 2))

        // If the cell is too little to display the box, or the line is too close to the height,
        // shows only the line.
        if ((2 * textSize.width) > (cellEndX - cellStartX) || (rpmY + boxHeight) >= (heightF + heightOffset)) {
            canvas.drawLine(
                cellStartX,
                rpmY,
                cellEndX,
                rpmY,
                Paint().apply {
                    this.strokeWidth = rpmSettings.rpmLineHeight
                    this.color = lineColor
                    if (isSelectedItem(index)) {
                        this.setShadowLayer(8f, 0f, 0f, lineColor)
                    }
                })

            return
        }

        canvas.drawLine(
            cellStartX,
            rpmY,
            cellStartX + rpmSettings.rpmBoxMarginStart,
            rpmY,
            Paint().apply {
                this.strokeWidth = rpmSettings.rpmLineHeight
                this.color = lineColor
                if (isSelectedItem(index)) {
                    this.setShadowLayer(8f, 0f, 0f, lineColor)
                }
            })

        canvas.drawRoundRect(
            cellStartX + rpmSettings.rpmBoxMarginStart,
            rpmY - (textSize.height / 2),
            cellStartX + rpmSettings.rpmBoxMarginStart + textSize.width,
            (rpmY - (textSize.height / 2)) + textSize.height,
            rpmSettings.rpmBoxRoundCorners,
            rpmSettings.rpmBoxRoundCorners,
            Paint().apply {
                this.isAntiAlias = true
                this.color = boxColor
                if (isSelectedItem(index)) {
                    this.setShadowLayer(8f, 0f, 0f, boxColor)
                }
            }
        )

        canvas.drawText(
            text,
            cellStartX + rpmSettings.rpmBoxMarginStart + rpmSettings.rpmBoxPadding,
            rpmY + (textSize.height / 2) - rpmSettings.rpmBoxPadding,
            textPaint
        )

        canvas.drawLine(
            cellStartX + rpmSettings.rpmBoxMarginStart + textSize.width,
            rpmY,
            cellEndX,
            rpmY,
            Paint().apply {
                this.strokeWidth = rpmSettings.rpmLineHeight
                this.color = lineColor
                if (isSelectedItem(index)) {
                    this.setShadowLayer(8f, 0f, 0f, lineColor)
                }
            })
    }
}