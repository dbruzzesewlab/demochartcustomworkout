package com.android.democustomworkoutchart

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.content.ContextCompat

object Utils {

    fun getBitmapFromVectorDrawable(
        context: Context,
        drawableId: Int,
        width: Int,
        height: Int
    ): Bitmap? {
        var bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        kotlin.runCatching {
            ContextCompat.getDrawable(context, drawableId)?.let {
                it.setBounds(0, 0, width, height)
                it.draw(canvas)
            }
        }.onFailure {
            bitmap = null
        }
        return bitmap
    }
}