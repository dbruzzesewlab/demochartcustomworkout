package com.android.democustomworkoutchart.data

import androidx.annotation.ColorInt

data class MaskOverFtp(
    @ColorInt val colorStart: Int,
    @ColorInt val colorEnd: Int,
    val alpha: Float
)