package com.android.democustomworkoutchart.widgets

import android.content.Context
import android.graphics.Rect
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import androidx.annotation.DrawableRes
import androidx.core.view.isVisible
import com.android.democustomworkoutchart.R
import com.android.democustomworkoutchart.data.GraphData
import com.android.democustomworkoutchart.data.MaskOverFtp
import com.android.democustomworkoutchart.data.RangeColor
import com.android.democustomworkoutchart.data.settings.*
import kotlin.math.max

class GraphLayout(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs),
    SliderView.OnSliderRangeChangedListener {

    private var graphView: GraphView
    private var sliderView: SliderView
    private var spaceBetweenChartAndSliderView: View
    private var spaceBetweenSliderAndCornerView: View
    private var rightCornerView: View

    init {
        inflate(context, R.layout.graph_layout, this)

        graphView = findViewById(R.id.graphView)
        sliderView = findViewById(R.id.sliderView)
        spaceBetweenChartAndSliderView = findViewById(R.id.spaceBetweenChartAndSliderView)
        spaceBetweenSliderAndCornerView = findViewById(R.id.spaceBetweenSliderAndCornerView)
        rightCornerView = findViewById(R.id.rightCornerView)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.GraphLayout)

        graphView.genericSettings = GenericSettings.fromTypedArray(typedArray)
        graphView.rpmSettings = RpmSettings.fromTypedArray(context, typedArray)
        graphView.ftpSettings = FtpSettings.fromTypedArray(context, typedArray)
        graphView.indexSettings = IndexSettings.fromTypedArray(context, typedArray)

        sliderView.sliderSettings = SliderSettings.fromTypedArray(typedArray)
        sliderView.onSliderRangeChangedListener = this

        setSpaceBetweenChartAndSlider(
            space = typedArray.getDimension(
                R.styleable.GraphLayout_gl_spaceBetweenChartAndSlider,
                0f
            )
        )

        setFtpTextVisible(graphView.ftpSettings.isFtpTextVisible)

        rightCornerView.setBackgroundColor(graphView.ftpSettings.ftpTextBackgroundColor)

        typedArray.recycle()
    }

    override fun setBackgroundColor(color: Int) {
        super.setBackgroundColor(color)
        rightCornerView.setBackgroundColor(color)
    }

    fun addPortion(graphData: GraphData) {
        graphView.addItem(graphData)
        sliderView.portions.add(graphData)
        graphView.totalTime += graphData.time
        sliderView.totalTime += graphData.time
        invalidate()
    }

    fun setRangeColors(vararg rangeColors: RangeColor) {
        graphView.rangeColors = rangeColors.toList()
        invalidate()
    }

    // region GenericSettings

    fun setSpaceBetweenItems(@Dimension space: Float) {
        graphView.genericSettings.spaceBetweenItems = space
        spaceBetweenSliderAndCornerView.layoutParams.width = space.toInt()
        invalidate()
    }

    fun setSpaceBetweenChartAndSlider(@Dimension space: Float) {
        (spaceBetweenChartAndSliderView.layoutParams as MarginLayoutParams).height = space.toInt()
        invalidate()
    }

    fun setAlternateBackgroundColors(@ColorInt color1: Int, @ColorInt color2: Int) {
        graphView.genericSettings.alternateBackgroundColor1 = color1
        graphView.genericSettings.alternateBackgroundColor2 = color2
        invalidate()
    }

    fun setDashHeight(@Dimension(unit = Dimension.DP) height: Float) {
        graphView.genericSettings.dashHeight = height
        invalidate()
    }

    fun setDashVisibleWidth(@Dimension(unit = Dimension.DP) width: Float) {
        graphView.genericSettings.dashVisibleWidth = width
        invalidate()
    }

    fun setDashSpacingWidth(@Dimension(unit = Dimension.DP) spacingWidth: Float) {
        graphView.genericSettings.dashSpacingWidth = spacingWidth
        invalidate()
    }

    fun setDashColor(@ColorInt color: Int) {
        graphView.genericSettings.dashColor = color
        invalidate()
    }

    fun setSelectedItemAtIndex(index: Int) {
        graphView.genericSettings.selectedItemAtIndex = index
        invalidate()
    }

    fun setSelectedColor(@ColorInt color: Int) {
        graphView.genericSettings.selectedColor = color
        invalidate()
    }

    fun setSelectedSeparatorColor(@ColorInt color: Int) {
        graphView.genericSettings.selectedSeparatorColor = color
        invalidate()
    }
    // endregion GenericSettings

    // region Index

    fun setIndexTypeface(typeface: String) {
        setIndexTypeface(
            kotlin.runCatching {
                Typeface.createFromAsset(context.assets, typeface)
            }.getOrNull()
        )
    }

    fun setIndexTypeface(typeface: Typeface?) {
        graphView.indexSettings.indexTypeface = typeface
        invalidate()
    }

    fun setIndexTextColor(@ColorInt color: Int) {
        graphView.indexSettings.indexTextColor = color
        invalidate()
    }

    fun setIndexSelectedTextColor(@ColorInt color: Int) {
        graphView.indexSettings.indexSelectedTextColor = color
        invalidate()
    }

    fun setIndexTextSize(@Dimension(unit = Dimension.SP) size: Float) {
        graphView.indexSettings.indexTextSize = size
        invalidate()
    }

    fun setIndexMarginTop(@Dimension(unit = Dimension.DP) margin: Float) {
        graphView.indexSettings.indexMarginTop = margin
        invalidate()
    }

    // endregion Index

    // region FTP

    fun setFtpText(text: String) {
        graphView.ftpSettings.ftpText = text
        setFtpTextVisible(graphView.ftpSettings.isFtpTextVisible)
        invalidate()
    }

    fun setFtpTypeface(typeface: String) {
        setFtpTypeface(
            kotlin.runCatching {
                Typeface.createFromAsset(context.assets, typeface)
            }.getOrNull()
        )
    }

    fun setFtpTypeface(typeface: Typeface?) {
        graphView.ftpSettings.ftpTypeface = typeface
        invalidate()
    }

    fun setFtpMaskOver(maskOverFtp: MaskOverFtp) {
        graphView.ftpSettings.ftpMaskOver = maskOverFtp
        invalidate()
    }

    fun showFtpStroke(@Dimension(unit = Dimension.DP) strokeWidth: Float) {
        graphView.ftpSettings.ftpStrokeWidth = strokeWidth
        graphView.ftpSettings.isFtpStrokeVisible = true
        invalidate()
    }

    fun setFtpAvg(avgFtp: Int) {
        graphView.ftpSettings.avgFtp = avgFtp
        invalidate()
    }

    fun setFtpTextColor(@ColorInt color: Int) {
        graphView.ftpSettings.ftpTextColor = color
        invalidate()
    }

    fun setFtpTextBackgroundColor(@ColorInt color: Int) {
        graphView.ftpSettings.ftpTextBackgroundColor = color
        rightCornerView.setBackgroundColor(color)
        invalidate()
    }

    fun setFtpTextLineSpacing(@Dimension spacing: Float) {
        graphView.ftpSettings.ftpTextLineSpacing = spacing
        setFtpTextVisible(graphView.ftpSettings.isFtpTextVisible)
        invalidate()
    }

    fun setFtpTextSize(size: Float) {
        graphView.ftpSettings.ftpTextSize = size
        setFtpTextVisible(graphView.ftpSettings.isFtpTextVisible)
        invalidate()
    }

    fun setFtpMaxPeak(maxFtpPeak: Int) {
        graphView.ftpSettings.maxFtpPeak = maxFtpPeak
        sliderView.maxFtpPeak = maxFtpPeak
        invalidate()
    }

    fun setFtpTextPadding(@Dimension(unit = Dimension.DP) padding: Float) {
        graphView.ftpSettings.ftpTextPadding = padding
        setFtpTextVisible(graphView.ftpSettings.isFtpTextVisible)
        invalidate()
    }

    fun setFtpTextVisible(visible: Boolean) {
        graphView.ftpSettings.isFtpTextVisible = visible
        if (visible) {
            graphView.ftpSettings.let {
                var maxTextWidth = 0
                var totalHeight = 0f
                val words = it.ftpText.split("\n")
                val bound = Rect()
                if (words.isNotEmpty()) {
                    it.ftpText.split("\n").forEach { t ->
                        it.ftpTextPaint.getTextBounds(t, 0, t.length, bound)
                        maxTextWidth = max(maxTextWidth, bound.width())
                        totalHeight += bound.height() + it.ftpTextLineSpacing
                    }
                } else {
                    it.ftpTextPaint.getTextBounds(it.ftpText, 0, it.ftpText.length, bound)
                    maxTextWidth = bound.width()
                    totalHeight = bound.height() + it.ftpTextLineSpacing
                }
                it.ftpTextWidthOffset = maxTextWidth.toFloat() + 2 * it.ftpTextPadding
                it.ftpTextHeightOffset = totalHeight
                rightCornerView.layoutParams.width = it.ftpTextWidthOffset.toInt()
            }
        } else {
            graphView.ftpSettings.let {
                it.ftpTextWidthOffset = 0f
                it.ftpTextHeightOffset = 0f
            }
            rightCornerView.isVisible = false
            spaceBetweenSliderAndCornerView.isVisible = false
        }
        invalidate()
    }

    // endregion FTP

    // region RPM

    fun setRpmTypeface(typeface: String) {
        setRpmTypeface(
            kotlin.runCatching {
                Typeface.createFromAsset(context.assets, typeface)
            }.getOrNull()
        )
    }

    fun setRpmTypeface(typeface: Typeface?) {
        graphView.rpmSettings.rpmTypeface = typeface
        invalidate()
    }

    fun setRpmMin(rpm: Int) {
        graphView.rpmSettings.rpmMin = rpm
        invalidate()
    }

    fun setRpmMax(rpm: Int) {
        graphView.rpmSettings.rpmMax = rpm
        invalidate()
    }

    fun setRpmLineColor(@ColorInt color: Int) {
        graphView.rpmSettings.rpmLineColor = color
        invalidate()
    }

    fun setRpmLineHeight(@Dimension height: Float) {
        graphView.rpmSettings.rpmLineHeight = height
        invalidate()
    }

    fun setRpmTextColor(@ColorInt color: Int) {
        graphView.rpmSettings.rpmTextColor = color
        invalidate()
    }

    fun setRpmTextSize(@Dimension size: Float) {
        graphView.rpmSettings.rpmTextSize = size
        invalidate()
    }

    fun setRpmBoxColor(@ColorInt color: Int) {
        graphView.rpmSettings.rpmBoxColor = color
        invalidate()
    }

    fun setRpmBoxRoundCorners(@Dimension(unit = Dimension.DP) size: Float) {
        graphView.rpmSettings.rpmBoxRoundCorners = size
        invalidate()
    }

    fun setRpmBoxMarginStart(@Dimension(unit = Dimension.DP) margin: Float) {
        graphView.rpmSettings.rpmBoxMarginStart = margin
        invalidate()
    }

    fun setRpmBoxPadding(@Dimension(unit = Dimension.DP) padding: Float) {
        graphView.rpmSettings.rpmBoxPadding = padding
        invalidate()
    }

    fun setRpmSelectedLineColor(@ColorInt color: Int) {
        graphView.rpmSettings.rpmSelectedLineColor = color
        invalidate()
    }

    fun setRpmSelectedBoxColor(@ColorInt color: Int) {
        graphView.rpmSettings.rpmSelectedBoxColor = color
        invalidate()
    }

    // endregion RPM

    // region Slider

    fun setSliderBackgroundColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderBackgroundColor = color
        invalidate()
    }

    fun setSliderBackgroundChartColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderBackgroundChartColor = color
        invalidate()
    }

    fun setSliderButtonLeftImageResource(@DrawableRes resource: Int) {
        sliderView.sliderSettings.sliderButtonLeftImageSrc = resource
        invalidate()
    }

    fun setSliderButtonRightImageResource(@DrawableRes resource: Int) {
        sliderView.sliderSettings.sliderButtonRightImageSrc = resource
        invalidate()
    }

    fun setSliderButtonLeftSelectedImageResource(@DrawableRes resource: Int) {
        sliderView.sliderSettings.sliderButtonLeftSelectedImageSrc = resource
        invalidate()
    }

    fun setSliderButtonRightSelectedImageResource(@DrawableRes resource: Int) {
        sliderView.sliderSettings.sliderButtonRightSelectedImageSrc = resource
        invalidate()
    }

    fun setSliderEdgeLeftColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderLeftEdgeColor = color
        invalidate()
    }

    fun setSliderEdgeRightColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderRightEdgeColor = color
        invalidate()
    }

    fun setSliderEdgeLeftSelectedColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderLeftEdgeSelectedColor = color
        invalidate()
    }

    fun setSliderEdgeRightSelectedColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderRightEdgeSelectedColor = color
        invalidate()
    }

    fun setSliderRangeAreaBackgroundColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderRangeAreaBackgroundColor = color
        invalidate()
    }

    fun setSliderRangeAreaPressedBackgroundColor(@ColorInt color: Int) {
        sliderView.sliderSettings.sliderRangeAreaPressedBackgroundColor = color
        invalidate()
    }

    fun setSliderTouchMode(mode: SliderSettings.SliderTouchMode) {
        sliderView.sliderSettings.sliderTouchMode = mode
        invalidate()
    }

    override fun onSliderRangeChanged(startRange: Float, endRange: Float) {
        graphView.updateRange(startRange, endRange)
    }

    // endregion Slider
}