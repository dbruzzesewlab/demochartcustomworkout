package com.android.democustomworkoutchart.data.settings

import android.content.res.TypedArray
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import com.android.democustomworkoutchart.R

class GenericSettings() {

    companion object {
        fun fromTypedArray(typedArray: TypedArray) = GenericSettings(typedArray)
    }

    @Dimension(unit = Dimension.DP)
    var spaceBetweenItems = 0f

    @ColorInt
    var alternateBackgroundColor1: Int = Color.TRANSPARENT

    @ColorInt
    var alternateBackgroundColor2: Int = Color.TRANSPARENT

    @Dimension(unit = Dimension.DP)
    var dashHeight = 0f

    @Dimension(unit = Dimension.DP)
    var dashVisibleWidth = 0f

    @Dimension(unit = Dimension.DP)
    var dashSpacingWidth = 0f

    @ColorInt
    var dashColor = Color.TRANSPARENT

    @ColorInt
    var selectedColor = Color.TRANSPARENT

    @ColorInt
    var selectedSeparatorColor = Color.TRANSPARENT

    var selectedItemAtIndex = -1

    private constructor(typedArray: TypedArray) : this() {

        alternateBackgroundColor1 = typedArray.getColor(
            R.styleable.GraphLayout_gl_alternateBackgroundColor1,
            alternateBackgroundColor1
        )

        alternateBackgroundColor2 = typedArray.getColor(
            R.styleable.GraphLayout_gl_alternateBackgroundColor2,
            alternateBackgroundColor2
        )

        dashHeight = typedArray.getDimension(
            R.styleable.GraphLayout_gl_dashHeight,
            dashHeight
        )

        dashVisibleWidth = typedArray.getDimension(
            R.styleable.GraphLayout_gl_dashVisibleWidth,
            dashVisibleWidth
        )

        dashSpacingWidth = typedArray.getDimension(
            R.styleable.GraphLayout_gl_dashSpacingWidth,
            dashSpacingWidth
        )

        dashColor = typedArray.getColor(R.styleable.GraphLayout_gl_dashColor, dashColor)

        spaceBetweenItems = typedArray.getDimension(
            R.styleable.GraphLayout_gl_spaceBetweenItems,
            spaceBetweenItems
        )

        selectedColor = typedArray.getColor(R.styleable.GraphLayout_gl_selectedColor, selectedColor)

        selectedSeparatorColor = typedArray.getColor(
            R.styleable.GraphLayout_gl_selectedSeparatorColor,
            selectedSeparatorColor
        )

        selectedItemAtIndex = typedArray.getColor(
            R.styleable.GraphLayout_gl_selectedItemAtIndex,
            selectedItemAtIndex
        )
    }
}